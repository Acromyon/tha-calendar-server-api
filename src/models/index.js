import { DataTypes } from 'sequelize'
import sequelize from '../db'

const User = sequelize.define('user', {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    img: {
        type: DataTypes.STRING,
    },
    name: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    email: {
        type: DataTypes.STRING,
        unique: true,
        allowNull: false,
    },
    password: {
        type: DataTypes.STRING,
        allowNull: false,
    },
})

const EventType = sequelize.define('event_type', {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    name: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    description: {
        type: DataTypes.STRING,
    },
    measure: {
        type: DataTypes.STRING,
    },
})

const Event = sequelize.define('event', {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    date: {
        type: DataTypes.DATEONLY,
        allowNull: false,
    },
    amount: {
        type: DataTypes.INTEGER,
        defaultValue: 1,
    },
})

User.hasMany(EventType)
EventType.belongsTo(User)

User.hasMany(Event, {
    foreignKey: {
    allowNull: false
  }
})
Event.belongsTo(User)

EventType.hasMany(Event, {
    foreignKey: {
    allowNull: false
  }
})
Event.belongsTo(EventType)

export { User, EventType, Event }
