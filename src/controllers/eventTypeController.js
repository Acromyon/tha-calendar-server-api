import ApiError from '../errors/ApiError'
import { EventType } from '../models'

class EventTypeController {
    async getAll(req, res, next) {
        try {
            const {
                userId, // TODO get current user from session
                limit = 15,
                page = 1
            } = req.query
            
            const offset = page * limit - limit
            
            const eventTypes = await EventType.findAll({
                where: { userId },
                limit,
                offset,
            })
            
            return res.json(eventTypes)
        } catch (err) {
            next(ApiError.badRequest(err.message))
        }
    }
    
    async getOne(req, res, next) {
        try {
            const { id } = req.params
            
            const eventType = await EventType.findOne({ where: { id } })
            
            // TODO check current user access rights
            
            return res.json(eventType)
        } catch (err) {
            next(ApiError.badRequest(err.message))
        }
    }
    
    async create(req, res, next) {
        try {
            const { name, description, measure, userId } = req.body
            
            const eventType = await EventType.create({
                name,
                description,
                measure,
                userId,
            })
            
            return res.json(eventType)
        } catch (err) {
            next(ApiError.badRequest(err.message))
        }
    }
}

export default new EventTypeController
