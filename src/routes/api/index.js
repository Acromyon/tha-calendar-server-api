import { Router } from 'express'

import userRouter from './userRouter'
import eventTypeRouter from './eventTypeRouter'
import eventRouter from './eventRouter'

const router = new Router()

router.use('/user', userRouter)
router.use('/event_type', eventTypeRouter)
router.use('/event', eventRouter)

export default router
