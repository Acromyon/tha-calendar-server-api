import { Router } from 'express'
import eventTypeController from '../../controllers/eventTypeController';

const router = new Router()

router.get('/', eventTypeController.getAll)
router.get('/:id', eventTypeController.getOne)
router.post('/', eventTypeController.create)

export default router
