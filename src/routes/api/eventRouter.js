import { Router } from 'express'
import eventController from '../../controllers/eventController';

const router = new Router()

router.get('/', eventController.getAll)
router.get('/:id', eventController.getOne)
router.post('/', eventController.create)

export default router
