FROM node:14-alpine as base
EXPOSE 8015
RUN npm i npm@latest -g

WORKDIR /app
COPY . .

RUN npm install
